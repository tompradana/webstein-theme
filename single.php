<?php get_header(); ?>

<div class="post-wrapper pt-5 pb-5">
    <div class="container">
        <div class="row">
            <div class="col">

            <?php while( have_posts() ) : the_post(); ?>

                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                    <header class="entry-header">
                        <?php the_title( '<h1 class="single-entry-title">', '</h1>' ); ?>
                    </header><!-- end .entry-header -->

                    <div class="single-entry-content">
                        <?php the_content(); ?>
                    </div><!-- end .entry-content -->

                </article><!-- end article -->

                <?php the_posts_navigation(); ?>

                <?php comments_template(); ?>

            <?php endwhile; ?>

            </div><!-- end .col -->
        </div><!-- end .row -->
    </div><!-- end .container -->
</div><!-- end .page-container -->

<?php get_footer();