<div id="mobile-menu" class="sidenav open">
    <a href="javascript:void(0)" class="close-button" onclick="closeNav()"><i class="fa-solid fa-xmark"></i></a>

    <div class="mobile-menu-container">
        <?php wp_nav_menu( [
            'container'         => 'nav',
            'menu_id'           => 'support-menu',
            'menu_class'        => 'menu list-no-style',
            'fallback_cb'       => false,
            'theme_location'    => 'mobile'
        ] ); ?>
    </div>
</div>

<div id="overlay"></div>