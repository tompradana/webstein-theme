<section id="process" class="pt-5 pb-5">
    <div class="container">
        <div class="row align-items-center justify-content-between">
            <div class="col-12 col-sm-5">
                <img class="img-fluid" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/art-2-webstein-theme.svg" alt="<?php esc_attr_e( "The unsenn", 'webstein-theme' ); ?>" />
            </div>
            <div class="col-12 col-sm-6">
                <h2><?php _e( 'How to design your site footer like <br/>we did', 'webstein-theme' ); ?></h2>
                <p>Donec a eros justo. Fusce egestas tristique ultrices. Nam tempor, augue nec tincidunt molestie, massa nunc varius arcu, at scelerisque elit erat a magna. Donec quis erat at libero ultrices mollis. In hac habitasse platea dictumst. Vivamus vehicula leo dui, at porta nisi facilisis finibus. In euismod augue vitae nisi ultricies, non aliquet urna tincidunt. Integer in nisi eget nulla commodo faucibus efficitur quis massa. Praesent felis est, finibus et nisi ac, hendrerit venenatis libero. Donec consectetur faucibus ipsum id gravida.</p>
                <a class="button" href="#"><?php _e( 'Learn More', 'webstein-theme' ); ?></a>
            </div>
        </div><!-- end .row -->
    </div><!-- end .container -->
</section><!-- end #process -->