<section id="hero" class="bg-grey">
    <div class="container owl-carousel">

        <div class="row pb-5 pt-5 align-items-center">
            <div class="col-8">
                <h1><?php _e( 'Lessons and insights <br/><span class="color-green">from 8 years</span>', 'webstein-theme' ); ?></h1>
                <h5 class="sub-title mt-3"><?php _e( 'Where to grow your business as a photographer: site or social media?', 'webstein-theme' ); ?></h5>
                <a href="" class="button mt-4"><?php _e( 'Register', 'webstein-theme' ); ?></a>
            </div>
            <div class="col-4">
                <img class="img-fluid" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/hero-illustration.png"/>
            </div>
        </div>

        <div class="row pb-5 pt-5 align-items-center">
            <div class="col-8">
                <h1><?php _e( 'Wisdom and Takeaways <br/><span class="color-green">from a Decade</span>', 'webstein-theme' ); ?></h1>
                <h5 class="sub-title mt-3"><?php _e( 'Where to grow your business as a photographer: site or social media?', 'webstein-theme' ); ?></h5>
                <a href="" class="button mt-4"><?php _e( 'Register', 'webstein-theme' ); ?></a>
            </div>
            <div class="col-4">
                <img class="img-fluid" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/hero-illustration.png"/>
            </div>
        </div>

        <div class="row pb-5 pt-5 align-items-center">
            <div class="col-8">
                <h1><?php _e( 'Key Learnings and Reflections <br/><span class="color-green">after 8 Years</span>', 'webstein-theme' ); ?></h1>
                <h5 class="sub-title mt-3"><?php _e( 'Where to grow your business as a photographer: site or social media?', 'webstein-theme' ); ?></h5>
                <a href="" class="button mt-4"><?php _e( 'Register', 'webstein-theme' ); ?></a>
            </div>
            <div class="col-4">
                <img class="img-fluid" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/hero-illustration.png"/>
            </div>
        </div>

    </div>
</section><!-- end #hero -->