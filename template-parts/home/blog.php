<section id="blog" class="pt-5 pb-0">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2 class="text-center"><?php _e( 'Caring is the new marketing', 'webstein-theme' ); ?></h2>
                <h5 class="sub-title text-center"><?php _e( "The Nextcent blog is the best place to read about the latest membership insights, <br/>trends and more. See who's joining the community, read about how our community <br/>are increasing their membership income and lot's more.", "webstein-theme" ); ?></h5>
            </div><!-- end .col -->
        </div><!-- end .row -->

        <div class="row mt-3">
            <?php
            $blog = new WP_Query( [
                'post_type'         => 'post',
                'posts_per_page'    => 3,
                'post_status'       => 'publish'
            ] );
            if ( $blog->have_posts() ) :
                while ( $blog->have_posts() ) : $blog->the_post(); ?>

                    <article id="post-<?php the_ID(); ?>" <?php post_class( 'col-12 col-sm-4' ); ?>>
                        <div class="entry">
                            <?php the_post_thumbnail( 'large', [ 'class' => 'img-fluid entry-thumbnail' ] ); ?>
                            <div class="entry-content p-4 text-center d-flex flex-column justify-content-between align-items-center">
                                <?php the_title( sprintf( '<h2 class="entry-title text-center mb-3"><a href="%s" title="%s">', esc_url( get_permalink() ), get_the_title() ), '</a></h2>' ); ?>
                                <a class="post-link color-green" href="<?php the_permalink(); ?>"><?php _e( 'Read more', 'webstein-theme' ); ?> <i class="fa-solid fa-arrow-right-long"></i></a>
                            </div><!-- end .entry-content -->
                        </div><!-- end .entry -->
                    </article><!-- end article -->
                <?php endwhile; ?>

            <?php else : ?>

                <?php _e( 'No posts found, please create your first blog!', 'webstein-theme' ); ?>

            <?php endif; wp_reset_query(); wp_reset_postdata(); ?>
        </div><!-- end .row -->
    </div><!-- end .container -->
</section><!-- end #blog -->