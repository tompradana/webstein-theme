<section id="about" class="pt-5 pb-5">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2 class="text-center"><?php _e( 'Manage your entire community<br/>in a single system', 'webstein-theme' ); ?></h2>
                <h5 class="sub-title text-center"><?php _e( 'Who is Nextcent suitable for?', 'webstein-theme' ); ?></h5>
            </div>
        </div>

        <div class="row justify-content-between pt-5">
            <div class="col-12 col-sm-4 col-md-3">
                <div class="text-center p-4 border rounded shadow">
                    <img src="<?php echo esc_url( get_template_directory_uri() );?>/assets/images/icon-blurb-1-webstein-theme.svg" width="75" alt="Blurb 1"/>
                    <h3 class="mt-3 mb-3"><?php _e( 'Membership Organisations', 'webstein-theme' ); ?></h3>
                    <p>Our membership management software provides full automation of membership renewals and payments</p>
                </div>
            </div>
            <div class="col-12 col-sm-4 col-md-3">
                <div class="text-center p-4 border rounded shadow">
                    <img src="<?php echo esc_url( get_template_directory_uri() );?>/assets/images/icon-blurb-2-webstein-theme.svg" width="75" alt="Blurb 2"/>
                    <h3 class="mt-3 mb-3"><?php _e( 'National Associations', 'webstein-theme' ); ?></h3>
                    <p><?php _e( 'Our membership management software provides full automation of membership renewals and payments', 'webstein-theme' ); ?></p>
                </div>
            </div>
            <div class="col-12 col-sm-4 col-md-3">
                <div class="text-center p-4 border rounded shadow">
                    <img src="<?php echo esc_url( get_template_directory_uri() );?>/assets/images/icon-blurb-3-webstein-theme.svg" width="75" alt="Blurb 3"/>
                    <h3 class="mt-3 mb-3"><?php _e( 'Clubs And <br/>Groups', 'webstein-theme' ); ?></h3>
                    <p><?php _e( 'Our membership management software provides full automation of membership renewals and payments', 'webstein-theme' ); ?></p>
                </div>
            </div>
        </div>

        <div class="row align-items-center justify-content-between">
            <div class="col-12 col-sm-5">
                <img class="img-fluid" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/art-1-webstein-theme.svg" alt="<?php esc_attr_e( "The unsenn", 'webstein-theme' ); ?>" />
            </div>
            <div class="col-12 col-sm-6">
                <h2><?php _e( 'The unseen of spending three years at Pixelgrade', 'webstein-theme' ); ?></h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet justo ipsum. Sed accumsan quam vitae est varius fringilla. Pellentesque placerat vestibulum lorem sed porta. Nullam mattis tristique iaculis. Nullam pulvinar sit amet risus pretium auctor. Etiam quis massa pulvinar, aliquam quam vitae, tempus sem. Donec elementum pulvinar odio.</p>
                <a class="button" href="#"><?php _e( 'Learn More', 'webstein-theme' ); ?></a>
            </div>
        </div>
    </div>
</section><!-- end #about -->