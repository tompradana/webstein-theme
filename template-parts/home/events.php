<section id="events" class="bg-grey pt-5 pb-5">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2 class="text-center"><?php _e( 'Upcoming Events', 'webstein-theme' ); ?></h2>
                <h5 class="sub-title text-center"><?php _e( 'Pariatur tempor irure nisi amet laborum aute.', 'webstein-theme' ); ?></h5>
            </div><!-- end .col -->
        </div><!-- end .row -->

        <div class="row">
            <div class="col">
                <?php echo do_shortcode( '[upcoming_events exclude_past_event="yes"]' ); ?>
                <div class="button-wrapper text-center">
                    <a class="button" href="/events/"><?php _e( 'View all Events', 'webstein-theme' ); ?> <i class="fa-solid fa-arrow-right-long"></i></a>
                </div>
            </div>
        </div><!-- end .row -->
    </div><!--end container -->
</section><!-- end #events -->