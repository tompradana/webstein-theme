<section id="clients" class="pt-5 pb-5">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2 class="text-center"><?php _e( 'Our Clients', 'webstein-theme' ); ?></h2>
                <h5 class="sub-title text-center"><?php _e( 'We have been working with some Fortune 500+ clients', 'webstein-theme' ); ?></h5>
            </div>
        </div><!-- end .row -->

        <div class="row pt-5">
            <div class="clients-logo col d-flex justify-content-between align-items-center">
                <?php for( $i = 1; $i <= 7; $i++ ) : 
                    $logo_number = ($i - 1) % 6 + 1;
                ?>
                    <img class="client-logo" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/clients-logo/logo-<?php echo $logo_number; ?>.svg" height="60" alt="<?php printf( __( 'Client Logo %s', 'webstein-theme' ), $logo_number ); ?>" />
                <?php endfor; ?>
            </div>
        </div><!-- end .row -->
    </div><!-- end .container -->
</section><!-- end #clients -->