<section id="achievements" class="bg-grey pt-5 pb-5">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-6 text-sm-center">
                <h2><?php _e( 'Helping a local', 'webstein-theme' ); ?><br/><span class="color-green"><?php _e( 'business reinvent itself', 'webstein-theme' ); ?></span></h2>
                <h5 class="sub-title"><?php _e( 'We reached here with our hard work and dedication', 'webstein-theme' ); ?></h5>
            </div><!-- end .col -->

            <div class="col-12 col-sm-12 col-md-6">
                <div class="row row-cols-2">
                    <?php 
                    $achievements_data = [
                        [
                            'count' => '2,245,341',
                            'label' => __( 'Members', 'webstein-theme' )
                        ],
                        [
                            'count' => '46,328',
                            'label' => __( 'Clubs', 'webstein-theme' )
                        ],
                        [
                            'count' => '828,867',
                            'label' => __( 'Event Bookings', 'webstein-theme' )
                        ],
                        [
                            'count' => '1,926,436',
                            'label' => __( 'Payments', 'webstein-theme' )
                        ]
                    ];
                    $i = 1; foreach( $achievements_data as $achievement ) : ?>
                    <div class="achievement col d-flex mb-3">
                        <img class="icon" width="48" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/achievement-icon-<?php echo $i; ?><?php if ( $i == 2 ) {?>.png<?php } else { ?>.svg<?php } ?>"/>
                        <div>
                            <h3 class="m-0"><?php echo esc_html( $achievement['count'] ); ?></h3>
                            <p class="m-0"><?php echo esc_html( $achievement['label'] ); ?></p>
                        </div>
                    </div>
                    <?php $i++; endforeach; ?>
                </div>
            </div><!-- end .col -->
        </div><!-- end .row -->
    </div><!-- end .container -->
</section><!-- end #achievements -->