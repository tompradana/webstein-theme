<section id="customers" class="bg-grey pt-5 pb-5">
    <div class="container">
        <div class="row align-items-center justify-content-between">
            <div class="col-12 col-md-4">
                <img class="img-fluid" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/customer-webstein-theme.png" alt="<?php esc_attr_e( "The customer", 'webstein-theme' ); ?>" />
            </div>
            <div class="col-12 col-md-7 text-sm-center">
                <p><blockquote>Maecenas dignissim justo eget nulla rutrum molestie. Maecenas lobortis sem dui, vel rutrum risus tincidunt ullamcorper. Proin eu enim metus. Vivamus sed libero ornare, tristique quam in, gravida enim. Nullam ut molestie arcu, at hendrerit elit. Morbi laoreet elit at ligula molestie, nec molestie mi blandit. Suspendisse cursus tellus sed augue ultrices, quis tristique nulla sodales. Suspendisse eget lorem eu turpis vestibulum pretium. Suspendisse potenti. Quisque malesuada enim sapien, vitae placerat ante feugiat eget. Quisque vulputate odio neque, eget efficitur libero condimentum id. Curabitur id nibh id sem dignissim finibus ac sit amet magna.</blockquote></p>
                <h5 class="color-green">Tim Smith</h5>
                <p class="color-light-grey">British Dragon Boat Racing Association</p>

                <div class="customers-logo d-flex align-items-center justify-content-between">
                    <?php for( $i = 1; $i <= 6; $i++ ) : ?>
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/clients-logo/logo-<?php echo $i; ?>.svg" height="40" alt="<?php printf( __( 'Client Logo %s', 'webstein-theme' ), $i); ?>" />
                    <?php endfor; ?>

                    <a href="#" class="icon-link color-green"><b><?php _e( 'Meet all customers', 'webstein-theme' ); ?></b> <i class="fa-solid fa-arrow-right-long"></i></a>
                </div><!-- end .customers-logo -->
            </div>
        </div><!-- end .row -->
    </div><!-- end .container -->
</section><!-- end #customers -->