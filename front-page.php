<?php get_header(); ?>

<?php  
    $home_modules = ['hero', 'clients', 'about', 'achievements', 'process', 'customers', 'blog', 'events'];
    foreach( $home_modules as $module ) {
        get_template_part( "template",  "parts/home/{$module}" );
    }
?>

<?php get_footer(); ?>