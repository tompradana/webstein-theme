<?php
/**
 * Setup the theme
 *
 * @return void
 */
function webstein_theme_setup() {
    // @https://developer.wordpress.org/reference/functions/add_theme_support/
    add_theme_support( 'title-tag' );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'automatic-feed-links' );
    register_nav_menus( [
        'main'      => __( 'Main Menu', 'webstein-theme' ),
        'company'   => __( 'Company Menu', 'webstein-theme' ),
        'support'   => __( 'Support Menu', 'webstein-theme' ),
        'mobile'    => __( 'Mobile Menu', 'webstein-theme' )
    ] );
}
add_action( 'after_setup_theme', 'webstein_theme_setup' );

/**
 * Enqueue scripts
 *
 * @return void
 */
function webstein_scripts() {
    $theme_dir_uri = esc_url( get_template_directory_uri() );

    // CSS
    wp_enqueue_style( 'gf-inter', '//fonts.googleapis.com/css2?family=Inter:wght@100..900&display=swap' );
    wp_enqueue_style( 'fa', $theme_dir_uri . '/assets/lib/fontawesome/css/fontawesome.min.css' );
    wp_enqueue_style( 'fa-brands', $theme_dir_uri . '/assets/lib/fontawesome/css/brands.min.css' );
    wp_enqueue_style( 'fa-solid', $theme_dir_uri . '/assets/lib/fontawesome/css/solid.min.css' );
    wp_enqueue_style( 'boostrap-reboot', $theme_dir_uri . '/assets/css/bootstrap-reboot.min.css', [], '5.3.3' );
    wp_enqueue_style( 'boostrap-grid', $theme_dir_uri . '/assets/css/bootstrap-grid.min.css', [], '5.3.3' );
    wp_enqueue_style( 'owlcarousel', $theme_dir_uri . '/assets/lib/owlcarousel/assets/owl.carousel.min.css', [], '2.3.4' );
    wp_enqueue_style( 'owlcarousel', $theme_dir_uri . '/assets/lib/owlcarousel/assets/owl.theme.default.min.css', [], '2.3.4' );
    wp_enqueue_style( 'webstein', get_stylesheet_uri() );

    // JavasScripts
    wp_enqueue_script( 'owlcarousel', $theme_dir_uri . '/assets/lib/owlcarousel/owl.carousel.min.js', ['jquery'], '2.3.4', true );
    wp_enqueue_script( 'webstein', $theme_dir_uri . '/assets/js/theme.js', ['jquery'], '1.0.0', true );

    // Comment reply link
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'webstein_scripts', 10 );

/**
 * Undocumented function
 *
 * @return void
 */
function webstein_gf_prefetch() {
    echo '<link rel="preconnect" href="https://fonts.googleapis.com">';
    echo '<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>';
}
add_action( 'wp_head', 'webstein_gf_prefetch', 5 );