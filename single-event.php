<?php get_header(); ?>

<div class="event-wrapper pt-5 pb-5">
    <div class="container">
        <div class="row">
            <div class="col">

            <?php while( have_posts() ) : the_post(); 
                $location   = get_post_meta( get_the_ID(), 'event_location', true ) != "" ? get_post_meta( get_the_ID(), 'event_location', true ) : __( "Online", "webstein-theme");
                $date       = get_post_meta( get_the_ID(), 'event_location', true ) != "" ? get_post_meta( get_the_ID(), 'event_date', true ) : date('Y-m-d');
                $eo         = get_post_meta( get_the_ID(), 'event_location', true ) != "" ? get_post_meta( get_the_ID(), 'event_organizer', true ) : __( "Event Organizer", "webstein-theme");
            ?>

                <article id="event-<?php the_ID(); ?>" <?php post_class(); ?>>

                    <header class="entry-header">
                        <?php the_title( '<h1 class="single-entry-title">', '</h1>' ); ?>
                        <div class="event-meta">
                            <div><i class="fa-solid fa-location-dot"></i> <?php echo esc_html( $location ); ?></div>
                            <div><i class="fa-solid fa-calendar-check"></i> <?php echo esc_html( date( 'F j, Y', strtotime( $date ) ) ); ?></div>
                            <div><i class="fa-regular fa-font-awesome"></i> <?php echo esc_html( $eo ); ?></div>
                        </div>
                    </header><!-- end .entry-header -->

                    <div class="single-entry-content">
                        <?php the_content(); ?>
                    </div><!-- end .entry-content -->

                </article><!-- end article -->

                <?php the_posts_navigation(); ?>

                <?php comments_template(); ?>

            <?php endwhile; ?>

            </div><!-- end .col -->
        </div><!-- end .row -->
    </div><!-- end .container -->
</div><!-- end .page-container -->

<?php get_footer();