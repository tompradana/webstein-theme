# Theme Installation Guide
Thank you for choosing our theme! This guide will help you install the theme by uploading the theme file to your WordPress site.

## Prerequisites
1. A self-hosted WordPress site. Requires at least: 6.0
2. Requires PHP: 7.1.0
3. Administrator access to your WordPress dashboard.

## Installation Steps
1. Log in to Your WordPress Admin Area
2. Navigate to the Themes Page, Admin > Appearance > Themes.
3. Upload the Theme, click the "Add New" button at the top of the page.
4. Click "Choose File" and select the theme .zip file from your computer.
5. Click the "Install Now" button.
6. Activate the Theme.

## Import Demo Content
1. Log in to Your WordPress Admin Area
2. Navigate to the Importer Page, Admin > Tools > Import.
3. The process requires WordPress importer, please install it first by clicking "Install Now".
4. Click "Run Importer"
5. Click "Choose File" button and select the demo_content.xml in webstein-theme/demo_content folder
6. Click "Upload file and import" button.

## How to activate Events Page Template
1. Create new page, Admin > Pages > Add New
2. Insert title
3. Insert custom fields and meta key name is: subtitle, meta value: Pariatur tempor irure nisi amet laborum aute.
4. Insert a shortcode [upcoming_events number="9" exclude_past_event="no" show_filter="yes"]
5. Publish the page.

## Troubleshooting
Upload Error: If you encounter an error during the upload, ensure that the file you are uploading is a .zip file.

## Activation Issues
If the theme doesn't activate properly, check for any plugin conflicts or ensure that your WordPress version is up to date.