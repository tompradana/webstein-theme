'use strict';

jQuery(document).ready(function ($) {
    // Owl carousel
    // See https://owlcarousel2.github.io/OwlCarousel2/docs/api-options.html
    $(".owl-carousel").owlCarousel({
        items: 1,
        loop: true
    });

    // simple off canvs menu
    $(".hamburger-menu").click(function () {
        $("#mobile-menu").animate({
            right: '0'
        }, 300); // 300 milliseconds for the animation duration
        $("#overlay").fadeIn(200); // Show overlay
        $("body").addClass("body-no-scroll"); // Disable body scrolling
    });

    $(".close-button, #overlay").click(function () {
        $("#mobile-menu").animate({
            right: '-250px'
        }, 300); // 300 milliseconds for the animation duration
        $("#overlay").fadeOut(200); // Hide overlay
        $("body").removeClass("body-no-scroll"); // Enable body scrolling
    });
});