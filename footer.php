    </main><!-- end #site-main -->
    
    <footer id="site-footer" class="pt-5 pb-5">
        <div class="container">
            
            <div class="row">
                <div class="col-12 col-sm-5">
                    <div class="widget widget-about">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/webstein-theme-logo-white.svg" width="150" height="26" alt="<?php _e( 'Webstein Theme Logo', 'webstein-theme' ); ?>"/>
                        <p class="mt-5 mb-5"><?php _e( 'Copyright © 2020 Landify UI Kit.', 'webstein-theme' ); ?><br/><?php _e( 'All rights reserved', 'webstein-theme' ); ?></p>
                        <ul class="social-networks list-no-style d-flex">
                            <li><a href="#"><i class="fa-brands fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fa-brands fa-dribbble"></i></a></li>
                            <li><a href="#"><i class="fa-brands fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa-brands fa-youtube"></i></a></li>
                        </ul>
                    </div><!-- end .widget -->
                </div><!-- end .col -->

                <div class="col-12 col-sm-2">
                    <div class="widget widget-menu">
                        <h5 class="widget-title"><?php _e( 'Company', 'webstein-theme' ); ?></h5>
                        <?php wp_nav_menu( [
                            'container'         => 'nav',
                            'menu_id'           => 'company-menu',
                            'menu_class'        => 'menu list-no-style',
                            'fallback_cb'       => false,
                            'theme_location'    => 'company'
                        ] ); ?>
                    </div><!-- end .widget -->
                </div><!-- end .col -->

                <div class="col-12 col-sm-2">
                    <div class="widget widget-menu">
                        <h5 class="widget-title"><?php _e( 'Support', 'webstein-theme' ); ?></h5>
                        <?php wp_nav_menu( [
                            'container'         => 'nav',
                            'menu_id'           => 'support-menu',
                            'menu_class'        => 'menu list-no-style',
                            'fallback_cb'       => false,
                            'theme_location'    => 'support'
                        ] ); ?>
                    </div><!-- end .widget -->
                </div><!-- end .col -->

                <div class="col-12 col-sm-3">
                    <div class="widget widget-subscription">
                        <h5 class="widget-title"><?php _e( 'Stay up to date', 'webstein-theme' ); ?></h5>
                        <form id="subscription-form" action="/thank-you" method="POST">
                            <div class="field">
                                <input type="email" placeholder="<?php esc_attr_e( 'Your email address', 'webstein-theme' ); ?>"/>
                            </div>
                        </form><!-- end #subscription-form -->
                    </div><!-- end .widget -->
                </div><!-- end .col -->
            </div><!-- end .row -->

        </div><!-- end .container -->
    </footer><!-- end #site-footer -->

    <?php get_template_part( 'template', 'parts/global/mobile-menu' ); ?>

    <?php wp_footer(); ?>
</body>
</html>