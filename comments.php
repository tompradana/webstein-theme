
<?php 
if ( post_password_required() ) {
	return;
};
$comments_number = get_comments_number();
$is_comment_open = comments_open();
?>
 
<div id="list-comments-<?php the_ID(); ?>" class="comments-area default-max-width <?php echo get_option( 'show_avatars' ) ? 'show-avatars' : ''; ?>">

    <?php if ( have_comments() ) : ?>

        <div id="comments-list" class="content">
            <h3 class="mb-6"><?php printf( _n( '%s Comment', '%s Comments', $comments_number, 'webstein-theme' ), number_format_i18n( $comments_number ) ); ?></h3>

            <ol>
                <?php wp_list_comments( [
                    'callback'    => 'webstein_list_comments',
                    'avatar_size' => 48,
					'style'       => 'ol',
					'short_ping'  => true,
                ] ); ?>
            </ol>
        </div>

    <?php else : ?>

    <?php endif; ?>

    <?php comment_form([
        'class_container'   => 'content',
        'class_submit'      => 'submit-comment mt-4'
    ]); ?>

</div>