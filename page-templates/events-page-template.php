<?php
/**
 * Template Name: Events
 */
get_header(); ?>

<section id="events" class="bg-grey pt-5 pb-5">
    <div class="container">

        <?php while( have_posts() ) : the_post(); ?>

        <div class="row">
            <div class="col">
                <h1 class="text-center"><?php the_title(); ?></h1>
                <?php if ( '' <> get_post_meta( get_the_ID(), 'subtitle', true ) ) : ?>
                    <h5 class="sub-title text-center"><?php echo esc_html( get_post_meta( get_the_ID(), 'subtitle', true ) ); ?></h5>
                <?php endif; ?>
            </div><!-- end .col -->
        </div><!-- end .row -->

        <div class="row mt-4">
            <div class="col">
                <?php the_content(); ?>
            </div>
        </div><!-- end .row -->

        <?php endwhile; ?>

    </div><!--end container -->
</section><!-- end #events -->

<?php get_footer();