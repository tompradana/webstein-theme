<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <?php wp_body_open(); ?>

    <header id="site-header" class="pt-3 pb-3">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-2">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/webstein-theme-logo.svg" width="150" height="26" alt="<?php _e( 'Webstein Theme Logo', 'webstein-theme' ); ?>"/>
                    </a>
                </div>

                <div class="col-10 d-flex align-items-center justify-content-end">
                    <?php wp_nav_menu( [
                        'container'         => 'nav',
                        'container_id'      => 'main-menu-navigation',
                        'container_class'   => 'd-none d-md-block',
                        'menu_id'           => 'main-menu',
                        'menu_class'        => 'menu list-no-style',
                        'fallback_cb'       => false,
                        'theme_location'    => 'main'
                    ] ); ?>

                    <a class="button d-none d-md-block" href="/">
                        <span><?php _e( 'Register Now', 'webstein-theme' ); ?> <i class="fa-solid fa-arrow-right-long"></i></span>
                    </a>

                    <a href="javascript://" class="hamburger-menu d-md-none">
                        <i class="fa-solid fa-bars"></i>
                    </a>
                </div>
            </div>
        </div><!-- end .container -->
    </header><!-- end #site-header -->

    <main id="site-main">